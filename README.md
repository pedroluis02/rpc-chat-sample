
# Chat RPC Sample
A sample chat using RPC (Remote Procedure Call) on SO Linux


#### Install RPC
```
sudo apt-get install rpcbind
```

#### Compile
```
make -f Makefile.chat
```

#### Run server
```
./chat_server
```

#### Run clients
```
./chat_client <IP_HOST>
```
