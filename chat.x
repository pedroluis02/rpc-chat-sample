
struct Usuario {
    int id;
    char nombre[50];
};

struct Nodo_U {
	struct Usuario usuario;
	struct Nodo_U *sgte;
};

struct Mensaje {
	char datos[255];
};

struct Mensaje_U {
    struct Usuario usuario;
    struct Mensaje mensaje;
};

struct Nodo_M {
	struct Mensaje_U men_usu;
	struct Nodo_M *sgte;
};

program CHAT {
    version CHAT_VERSION {
        Usuario conexion(Usuario) = 1;
        int desconexion(int) = 2; /*envia id de usuario*/
        int setMensaje(Mensaje_U) = 3;
        /**/
        Nodo_U listaUsuarios(int) = 4; /*envia id de usuario*/
        Nodo_M mensajes(int) = 5; /*envia id de usuario*/
    } = 1;
} = 2013;